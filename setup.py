from setuptools import setup
import os

setup(
    name="cf.pyutils",
    packages=["pyutils"],
    include_package_data=True,
    install_requires=[],
    version=os.environ.get('CI_COMMIT_TAG', "0.1.3"),
    url="https://gitlab.com/MatthieuJacquemet/pyutils",
    author="Matthieu Jacquemet",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
